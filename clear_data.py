import pandas as pd
import os
import csv


def wrapping_buckets(s):
    if pd.isnull(s):
        return s
    else:
        return f'\"{s}\"'


def clear_file(path_middle: str, path_end: str):
    files = [file for file in os.listdir(path_middle) if not file.startswith('.')]
    all_data = pd.DataFrame()
    count = 0
    for file in files:
        print(f'{path_middle}\{file}')
        if len(all_data) <= 500000:
            current_data = pd.read_csv(f'{path_middle}\{file}', sep=';', encoding='utf-8', dtype={0: 'string', 1: 'string',
                                                                                           2: 'string', 3: 'int',
                                                                                           5: 'string', 6: 'string',
                                                                                           8: 'string', 10: 'string',
                                                                                           11: 'string', 12: 'string',
                                                                                           14: 'string', 15: 'string',
                                                                                           16: 'string', 17: 'int'})
            all_data = pd.concat([all_data, current_data])
        else:
            count += 1
            print(f'Количество общих записей до удаления дубликатов: {all_data.count()[0]}')
            all_data.drop_duplicates(
                subset=['birthdate', 'code_mkb', 'date_diagnosis', 'firstname', 'lastname', 'number', 'series']
            )
            all_data.sort_index(inplace=True)
            print(f'Количество общих записей после удаления дубликатов: {all_data.count()[0]}')
            all_data = all_data.applymap(wrapping_buckets)
            all_data.to_csv(f'{path_end}\clear_data_{count}.csv', sep=';', index=False, quoting=csv.QUOTE_NONE,
                            float_format='%.12g')
            all_data = pd.DataFrame()

    print(f'Количество общих записей до удаления дубликатов: {all_data.count()[0]}')
    all_data.drop_duplicates(
        subset=['birthdate', 'code_mkb', 'date_diagnosis', 'firstname', 'lastname', 'number', 'series']
    )
    all_data.sort_index(inplace=True)
    print(f'Количество общих записей после удаления дубликатов: {all_data.count()[0]}')
    all_data = all_data.applymap(wrapping_buckets)
    all_data.to_csv(f'{path_end}\clear_data_{count + 1}.csv', sep=';', index=False, quoting=csv.QUOTE_NONE,
                    float_format='%.12g')
