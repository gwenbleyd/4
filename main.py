import requests, random, string, csv, re, time, os
from datetime import datetime, timedelta
import pandas as pd
from count import counting_records
from clear_data import clear_file
from zip_file import zip_file
import config

start_time = time.time()

start_date = datetime.strftime(datetime.now() - timedelta(30), '%Y-%m-%d 00:00:01')
print(start_date)
end_date = datetime.strftime(datetime.now(), '%Y-%m-%d 00:00:01')

path = datetime.strftime(datetime.now(), '%Y-%m-%d')
path_finally = config.FINALL_PATH
path_start = f'{path}/Начальный файл'
path_middle = f'{path}/Промежуточный файл'
path_end = f'{path}/Конечный файл файл'


def create_path():
    if not os.path.exists(path):
        os.makedirs(path)
    if not os.path.exists(path_finally):
        os.makedirs(path_finally)
    if not os.path.exists(path_start):
        os.makedirs(path_start)
    if not os.path.exists(path_middle):
        os.makedirs(path_middle)
    if not os.path.exists(path_end):
        os.makedirs(path_end)


def check_birthdate(date):
    birthdate = pd.to_datetime(date)
    today = datetime.today()
    age = today.year - birthdate.year
    if age < 17 or age > 120:
        return pd.NaT
    else:
        return date


def check_birthdate_date_issue(dates):
    date_issue = pd.to_datetime(dates['date_issue'])
    birthdate = pd.to_datetime(dates['birthdate'])
    if (date_issue - timedelta(14*366)) < birthdate:
        return pd.NaT
    else:
        return date


def check_date_issue(dates):
    date_issue = pd.to_datetime(dates['date_issue'])
    birthdate = pd.to_datetime(dates['birthdate'])
    today = datetime.today()
    if (date_issue - timedelta(14*366)) < birthdate or date_issue > today:
        return pd.NaT
    else:
        return date_issue


def clean_string(s):
    s = str(s)
    s = re.sub(r"[^\w\s-]", "", s)
    s = re.sub(r"\s+", " ", s)
    return s


def remove_punctuation(s):
    s = str(s)
    pattern_1 = r'[().,!&?@()=]'
    pattern_2 = r'[_-]'
    s = re.sub(pattern_1, '', s)
    s = re.sub(pattern_2, ' ', s)
    return s


def remove_quote(s):
    pattern = r'["]'
    s = re.sub(pattern, '\'', s)
    return s


def remove_space(s):
    try:
        return re.sub(' ', '', s)
    except:
        return pd.NaT


def wrapping_buckets(s):
    if pd.isnull(s):
        return s
    else:
        return f'\"{s}\"'


def normalize_snils(snils):
    snils = str(''.join(i for i in snils if i.isdigit()))
    while len(snils) < 11:
        snils = '0' + snils
    snils = f'{snils[0:3]}-{snils[3:6]}-{snils[6:9]} {snils[9:11]}'
    return snils


def check_snils(snils):
    snils = normalize_snils(snils)

    if len(snils) != 14:
        return pd.NaT

    def snils_csum(snils):
        k = range(9, 0, -1)
        pairs = zip(k, [int(x) for x in snils.replace('-', '').replace(' ', '')[:-2]])
        return sum([k * v for k, v in pairs])

    csum = snils_csum(snils)

    while csum > 101:
        csum %= 101
    if csum in (100, 101):
        csum = 0

    if csum == int(snils[-2:]):
        return snils
    else:
        snils = snils[:-2] + str(snils[-2:])
        return snils


def convert_snils_format(snils):
    try:
        snils = check_snils(snils)
        return snils
    except:
        return pd.NaT


def convert_pass_number(n):
    try:
        number = re.sub(" ", "", str(n))
        while len(number) < 6:
            number = '0' + number
        if re.match(r'^(?!(0{6})|(\D*0{6}))\d{6}$', number):
            return number
        else:
            return pd.NaT
    except:
        return pd.NaT


def convert_pass_series(d):
    try:
        series = re.sub(" ", "", str(d))
        while len(series) < 4:
            series = '0' + series
        if re.match(r'\A(?!0000)(?=\d\d\d\d)\d\d\d\d\Z', series):
            return series
        else:
            return pd.NaT
    except:
        return pd.NaT


def convert_oms(s):
    try:
        oms = str(''.join(i for i in s if i.isdigit()))
        if len(oms) == 16:
            return oms
        else:
            return pd.NaT
    except:
        return pd.NaT


def split_fio(s):
    s = clean_string(s)
    fio = s.split(' ', maxsplit=2)
    try:
        lastname = fio[0]
    except:
        lastname = pd.NaT
    try:
        firstname = fio[1]
    except:
        firstname = pd.NaT
    try:
        middlename = fio[2]
    except:
        middlename = pd.NaT
    return lastname, firstname, middlename


def check_named(s):
    s = str(s)
    phrase_with_words = fr'[а-яА-Я\s\xA0\-ёЁ`]+[а-яА-Я\s\xA0\-ёЁ`\.][а-яА-Я\s\xA0\-ёЁ`]+|[а-яА-Я\s\xA0\-ёЁ`]{2,}\.|[а-яА-Я\s\xA0\-ёЁ`]+'
    norm_name = re.match(phrase_with_words, s)
    if norm_name:
        norm_name = str(norm_name.group())
        pattern_1 = r'[().,!&?@()=]'
        pattern_2 = r'[_-]'
        norm_name = re.sub(pattern_1, '', norm_name)
        norm_name = re.sub(pattern_2, ' ', norm_name)
        norm_name = re.sub(r"[^\w\s-]", "", norm_name)
        norm_name = re.sub(r"\s+", " ", norm_name)
        norm_name = re.sub(" ", "", norm_name)
        if len(s) < 2:
            return pd.NaT
        else:
            return norm_name


def date_conversion(date):
    try:
        return pd.to_datetime(date).strftime('%Y-%m-%d')
    except:
        return pd.NaT


def convert_gender(s):
    if s == 'female':
        return s
    elif s == 'male':
        return s
    else:
        return pd.NaT


def prepare_file():
    df = pd.read_csv(f'{path_start}/raw_file.csv', sep=';',
                     dtype={3: 'string', 5: 'string', 6: 'string'})
    df.replace('', pd.NaT, inplace=True)
    df.dropna(subset=['case_patient_pasport_date_begin', 'case_patient_name', 'case_patient_birthdate'])
    df['lastname'], df['firstname'], df['middlename'] = zip(*df['case_patient_name'].apply(split_fio))
    df['lastname'] = df['lastname'].apply(check_named)
    df['firstname'] = df['firstname'].apply(check_named)
    df['middlename'] = df['middlename'].apply(check_named)
    df[['series', 'number']] = df.pop('case_patient_pasport_number').str.split(':', expand=True)
    df['series'] = df['series'].apply(remove_space)
    df['number'] = df['number'].apply(remove_space)
    df['series'] = df['series'].apply(convert_pass_series)
    df['number'] = df['number'].apply(convert_pass_number)
    df['birthdate'] = df['case_patient_birthdate'].apply(date_conversion)
    df['birthdate'] = df['birthdate'].apply(check_birthdate)
    df['date_issue'] = df['case_patient_pasport_date_begin'].apply(date_conversion)
    df['date_issue'] = df[['date_issue', 'birthdate']].apply(check_date_issue, axis=1)
    df['date_issue'] = df['date_issue'].apply(date_conversion)
    df['date_diagnosis'] = df['diagnosis_diagnoseddate'].apply(date_conversion)
    df['snils'] = df['case_patient_snils_number'].apply(convert_snils_format)
    df['case_patient_gender'] = df['case_patient_gender'].apply(convert_gender)
    df['gender'] = df['case_patient_gender'].replace({"female": int(2), "male": int(1), pd.NaT: int(0)})
    df['gender'] = df['gender'].fillna(0)
    df['oms_number'] = df['case_patient_policy_unite_number'].apply(convert_oms)
    df['ern_id'] = pd.NaT
    df['disease_name'] = df['diagnosis_mkb_namecod']
    df['disease_name'] = df['disease_name'].apply(remove_quote)
    df['code_mkb'] = df['diagnosis_mkb_code4']
    df['lastname_medic'] = df['diagnosis_doctor_family_name']
    df['lastname_medic'] = df['lastname_medic'].apply(check_named)
    df['firstname_medic'] = df['diagnosis_doctor_given_name']
    df['firstname_medic'] = df['firstname_medic'].apply(check_named)
    df['middlename_medic'] = df['diagnosis_doctor_middle_name']
    df['middlename_medic'] = df['middlename_medic'].apply(check_named)
    df['tag_mkb'] = 1
    df[[
        'service_info_quality',
        'service_status_quality',
        'service_timestamp_quality',
        'lastname_norm',
        'firstname_norm',
        'middlename_norm',
        'birthdate_norm',
        'series_norm',
        'number_norm',
        'snils_norm',
        'lastname_norm_hash',
        'firstname_norm_hash',
        'middlename_norm_hash',
        'birthdate_norm_hash',
        'series_norm_hash',
        'number_norm_hash',
        'snils_norm_hash',
        'fio_norm_hash'
    ]] = pd.NaT
    df.drop(columns=['case_patient_gender', 'case_patient_birthdate', 'case_patient_pasport_date_begin',
                     'case_patient_snils_number', 'case_patient_policy_unite_number', 'diagnosis_mkb_namecod',
                     'diagnosis_mkb_code4', 'diagnosis_diagnoseddate', 'diagnosis_doctor_family_name',
                     'diagnosis_doctor_given_name', 'diagnosis_doctor_middle_name'], inplace=True)

    df = df[
        ['lastname', 'firstname', 'middlename', 'gender', 'birthdate', 'series', 'number', 'date_issue', 'snils',
         'ern_id', 'oms_number', 'disease_name', 'code_mkb', 'date_diagnosis', 'lastname_medic', 'firstname_medic',
         'middlename_medic', 'tag_mkb', 'service_info_quality', 'service_status_quality',
         'service_timestamp_quality',
         'lastname_norm', 'firstname_norm', 'middlename_norm', 'birthdate_norm', 'series_norm', 'number_norm',
         'snils_norm', 'lastname_norm_hash', 'firstname_norm_hash', 'middlename_norm_hash', 'birthdate_norm_hash',
         'series_norm_hash', 'number_norm_hash', 'snils_norm_hash', 'fio_norm_hash']]

    df.replace('', pd.NaT, inplace=True)
    df.dropna(
        subset=['lastname', 'firstname', 'birthdate', 'series', 'number', 'date_issue', 'disease_name', 'code_mkb',
                'date_diagnosis', 'lastname_medic', 'firstname_medic', 'tag_mkb'], inplace=True)

    df = df.drop_duplicates(
        subset=['birthdate', 'code_mkb', 'date_diagnosis', 'firstname', 'lastname', 'number', 'series'])
    df.sort_index(inplace=True)

    df = df.applymap(wrapping_buckets)
    df.to_csv(f'{path_middle}/clear_file_{count}.csv', sep=';', index=False, quoting=csv.QUOTE_NONE,
              float_format='%.12g')


def generate_random_string(length):
    characters = string.ascii_letters + string.digits + '-'
    return ''.join(random.choice(characters) for i in range(length))


count_record = counting_records(generate_random_string(10), start_date, end_date)
print(count_record)
quotient = int(count_record / 60000) + 1

session = requests.Session()
sql_list = []
for n in range(quotient):
    sql_list.append(
        f"""
            SELECT DISTINCT
                case_patient_name,
                case_patient_gender,
                case_patient_birthdate,
                case_patient_pasport_number,
                case_patient_pasport_date_begin,
                case_patient_snils_number,
                case_patient_policy_unite_number,
                diagnosis_mkb_namecod,
                diagnosis_mkb_code4,
                diagnosis_diagnoseddate,
                diagnosis_doctor_family_name,
                diagnosis_doctor_given_name,
                diagnosis_doctor_middle_name
            FROM
                iemk_diagnosis_mart
            WHERE
                diagnosis_mkb_code4 BETWEEN 'E34.3%' AND 'E34.4%'
                OR diagnosis_mkb_code4 BETWEEN 'E66%' AND 'E77.99%'
                OR diagnosis_mkb_code4 BETWEEN 'E68%' AND 'E68.99%'
                OR diagnosis_mkb_code4 BETWEEN 'E70%' AND 'E90.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F00%' AND 'F07.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F09%' AND 'F10.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F11%' AND 'F19.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F20%' AND 'F25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F28%' AND 'F30.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F31%' AND 'F31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F32%' AND 'F34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F40%' AND 'F48.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F54%' AND 'F54.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F60%' AND 'F60.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F61%' AND 'F65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F68%' AND 'F69.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F70%' AND 'F70.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F71%' AND 'F73.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F78%' AND 'F79.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F80%' AND 'F80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F98%' AND 'F98.99%'
                OR diagnosis_mkb_code4 BETWEEN 'F99%' AND 'F99.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G10%' AND 'G13.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G20%' AND 'G26.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G30%' AND 'G32.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G35%' AND 'G36.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G37%' AND 'G37.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G40%' AND 'G40.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G41%' AND 'G41.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G43%' AND 'G46.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G71%' AND 'G71.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G72%' AND 'G73.99%'
                OR diagnosis_mkb_code4 BETWEEN 'G90%' AND 'G98.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H15%' AND 'H22.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H25%' AND 'H28.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H30%' AND 'H31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H34%' AND 'H35.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H40%' AND 'H40.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H42%' AND 'H42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H43%' AND 'H48.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H49%' AND 'H51.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H52%' AND 'H53.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H54%' AND 'H54.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H57%' AND 'H57.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H58%' AND 'H59.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H60%' AND 'H61.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H65%' AND 'H74.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H75%' AND 'H75.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H80%' AND 'H80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H81%' AND 'H82.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H83%' AND 'H83.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H90%' AND 'H91.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H93%' AND 'H93.99%'
                OR diagnosis_mkb_code4 BETWEEN 'H94%' AND 'H95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I01%' AND 'I09.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I10%' AND 'I15.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I20%' AND 'I25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I27%' AND 'I28.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I30%' AND 'I31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I33%' AND 'I42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I44%' AND 'I44.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I45%' AND 'I51.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I60%' AND 'I69.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I70%' AND 'I83.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I84%' AND 'I84.99%'
                OR diagnosis_mkb_code4 BETWEEN 'I85%' AND 'I89.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J12%' AND 'J18.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J30%' AND 'J34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J30%' AND 'J37.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J38%' AND 'J39.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J40%' AND 'J42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J43%' AND 'J44.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J45%' AND 'J47.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J60%' AND 'J65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J66%' AND 'J70.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J80%' AND 'J80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J81%' AND 'J81.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J84%' AND 'J84.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J85%' AND 'J86.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J90%' AND 'J90.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J93%' AND 'J94.99%'
                OR diagnosis_mkb_code4 BETWEEN 'J95%' AND 'J98.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K00%' AND 'K01.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K04%' AND 'K06.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K07%' AND 'K10.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K12%' AND 'K12.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K14%' AND 'K14.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K20%' AND 'K22.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K25%' AND 'K31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K40%' AND 'K46.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K50%' AND 'K63.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K66%' AND 'K67.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K70%' AND 'K76.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K80%' AND 'K86.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K91%' AND 'K91.99%'
                OR diagnosis_mkb_code4 BETWEEN 'K92%' AND 'K92.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L00%' AND 'L08.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L10%' AND 'L12.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L40%' AND 'L41.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L51%' AND 'L54.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L63%' AND 'L65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L80%' AND 'L80.99%'
                OR diagnosis_mkb_code4 BETWEEN 'L92%' AND 'L95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M00%' AND 'M19.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M22%' AND 'M25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M30%' AND 'M35.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M60%' AND 'M63.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M65%' AND 'M67.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M70%' AND 'M72.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M75%' AND 'M77.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M80%' AND 'M89.99%'
                OR diagnosis_mkb_code4 BETWEEN 'M93%' AND 'M94.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N01%' AND 'N05.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N10%' AND 'N19.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N20%' AND 'N23.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N25%' AND 'N25.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N30%' AND 'N30.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N32%' AND 'N32.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N34%' AND 'N34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N39%' AND 'N39.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N40%' AND 'N45.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N48%' AND 'N49.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N70%' AND 'N76.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N80%' AND 'N85.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N89%' AND 'N89.99%'
                OR diagnosis_mkb_code4 BETWEEN 'N91%' AND 'N95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'O00%' AND 'O99.99%'
                OR diagnosis_mkb_code4 BETWEEN 'P33%' AND 'P33.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q16%' AND 'Q17.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q18%' AND 'Q18.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q20%' AND 'Q21.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q22%' AND 'Q28.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q30%' AND 'Q31.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q32%' AND 'Q34.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q38%' AND 'Q38.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q39%' AND 'Q43.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q44%' AND 'Q45.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q50%' AND 'Q52.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q55%' AND 'Q56.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q60%' AND 'Q63.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q64%' AND 'Q64.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q67%' AND 'Q67.44%'
                OR diagnosis_mkb_code4 BETWEEN 'Q75%' AND 'Q75.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q80%' AND 'Q85.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Q87.1%' AND 'Q87.2%'
                OR diagnosis_mkb_code4 BETWEEN 'R32%' AND 'R32.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R42%' AND 'R42.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R44%' AND 'R44.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R47%' AND 'R49.99%'
                OR diagnosis_mkb_code4 BETWEEN 'R62%' AND 'R62.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S00%' AND 'S01.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S02%' AND 'S05.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S03%' AND 'S03.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S04%' AND 'S04.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S06%' AND 'S08.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S09%' AND 'S10.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S12%' AND 'S12.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S26%' AND 'S29.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S36%' AND 'S39.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S48%' AND 'S48.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S58%' AND 'S58.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S68%' AND 'S68.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S78%' AND 'S78.99%'
                OR diagnosis_mkb_code4 BETWEEN 'S88%' AND 'S88.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T34%' AND 'T35.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T36%' AND 'T65.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T90%' AND 'T90.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T91%' AND 'T94.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T95%' AND 'T95.99%'
                OR diagnosis_mkb_code4 BETWEEN 'T96%' AND 'T97.99%'
                OR diagnosis_mkb_code4 BETWEEN 'Z89%' AND 'Z89.99%'
                AND case_fiemk_date >= toDateTime('{start_date}')
                AND case_fiemk_date <= toDateTime('{end_date}')
                AND case_doctor_family_name != 'NULL'
                AND case_doctor_given_name != 'NULL'
                AND case_patient_name != 'NULL'
                AND case_patient_pasport_date_begin != 'NULL'
                AND case_patient_birthdate != 'NULL'
                AND diagnosis_diagnoseddate != 'NULL'
                AND ((toYear(toDateTime(case_patient_pasport_date_begin))-14) > toYear(toDateTime(case_patient_birthdate)))
                AND toYear(toDateTime(case_patient_pasport_date_begin)) < toYear(toDateTime(now()))
                LIMIT 60000 OFFSET ({n} * 60000)
        """
    )

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                         'Chrome/102.0.0.0 Safari/537.36'}

login_data = {
    'username': config.USERNAME,
    'password': config.PASSWORD
}

request = session.post(f'{config.URL}/login/', data=login_data, headers=headers)

count = 0

create_path()

for sql in sql_list:
    count += 1
    with open(f'{path_start}/raw_file.csv', 'wb+') as f:
        client_id = generate_random_string(10)
        data = {
            'client_id': client_id,
            'database_id': '2',
            'schema': 'r47',
            'sql': sql
        }
        report = f'{config.URL}/superset/sql_json/'

        try:
            response = session.post(report, data=data)
        except requests.exceptions.ConnectionError as e:
            print('ConnectionError')
            exit(1)

        response = session.get(f'{config.URL}/superset/csv/{client_id}')
        f.write(response.content)
    prepare_file()
    print(f"{count}-файл: %s seconds" % (time.time() - start_time))

clear_file(path_middle, path_end)
zip_file(path, path_end, path_finally)
print("Общее время: %s seconds" % (time.time() - start_time))
