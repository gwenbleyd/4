import zipfile
import pathlib


def zip_file(path: str, path_end: str, path_finally: str):
    directory = pathlib.Path(f"{path_end}/")
    with zipfile.ZipFile(f"{path_finally}/Выгрузка по СМЭВ4 на {path}.zip", mode="w") as archive:
        for file_path in directory.iterdir():
            archive.write(file_path, arcname=file_path.name)
