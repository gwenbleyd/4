import requests, re
import config
import logging

def counting_records(client_id, start, end) -> int:
    session = requests.Session()

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36'}

    login_data = {
        'username': config.USERNAME,
        'password': config.PASSWORD
    }

    request = session.post(f'{config.URL}/login/', data=login_data, headers=headers)

    sql_request = f"""
        SELECT 
            COUNT()
        FROM 
            iemk_diagnosis_mart
        WHERE
            case_fiemk_date >= toDateTime('{start}')
            AND case_fiemk_date <= toDateTime('{end}')
            AND case_doctor_family_name != 'NULL'
            AND case_doctor_given_name != 'NULL'
            AND case_patient_name != 'NULL'
            AND case_patient_pasport_date_begin != 'NULL'
            AND case_patient_birthdate != 'NULL'
            AND diagnosis_diagnoseddate != 'NULL'
            AND ((toYear(toDateTime(case_patient_pasport_date_begin))-14) > toYear(toDateTime(case_patient_birthdate)))
            AND toYear(toDateTime(case_patient_pasport_date_begin)) < toYear(toDateTime(now()))
        LIMIT 1000
    """

    data = {
        'client_id': client_id,
        'database_id': '2',
        'schema': 'r47',
        'sql': sql_request
    }

    logging.basicConfig(filename='logfile.log', level=logging.DEBUG)
    report = f'{config.URL}/superset/sql_json/'
    try:
        response = session.post(report, data=data)
    except requests.exceptions.ConnectionError as e:
        print('ConnectionError')
        exit(0)

    response = session.get(f'{config.URL}/superset/csv/{client_id}')
    number_of_entries = int(re.findall(r'\d+', response.content.decode("utf-8"))[0])

    return number_of_entries
